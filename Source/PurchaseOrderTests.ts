/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {ProcurifyAPI} from './procurifyapi';
import * as log from 'N/log';

export const performPurchaseOrderTests = (procurifyAPI: ProcurifyAPI) => {
    procurifyAPI.getPurchaseOrders({
        status: 'synced',
        OK: purchaseOrders => {
            purchaseOrders.forEach(purchaseOrder => {
                log.debug('getPurchaseOrders', purchaseOrder);
            });
        },
        Failed: response => {
            log.error('getPurchaseOrders - Failed', response);
        }
    });
};
