/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {ProcurifyAPI} from './procurifyapi';
import * as log from 'N/log';

export const performItemReceiptLinesTests = (procurifyAPI: ProcurifyAPI) => {
    procurifyAPI.getItemReceiptLines({
        status: 'synced',
        OK: itemReceiptLines => {
            itemReceiptLines.forEach(itemReceiptLine => {
                log.debug('getItemReceiptLines', itemReceiptLine);
            });
        },
        Failed: response => {
            log.error('getItemReceiptLines - Failed', response);
        }
    });
};
