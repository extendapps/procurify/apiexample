/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {ProcurifyAPI} from './procurifyapi';
import * as log from 'N/log';

export const performContentTypeTests = (procurifyAPI: ProcurifyAPI) => {
    const models: string [] = ['po', 'item'];
    const appLabels: string [] = ['procurify', 'receipt'];

    models.forEach(model => {
        procurifyAPI.getContentTypes({
            model: <'po'>model,
            OK: contentTypes => {
                contentTypes.forEach(contentType => {
                    log.debug(`getContentTypes - contentType (${model})`, contentType);

                    appLabels.forEach(appLabel => {
                        procurifyAPI.getContentType({
                            model: <'po'>model,
                            app_label: <'receipt'>appLabel,
                            OK: foundContentType => {
                                log.debug(`getContentType - contentType (${model}:${appLabel})`, foundContentType);
                            },
                            Failed: response => {
                                log.error(`getContentType (${model}:${appLabel}) - Failed`, response);
                            }
                        });
                    });
                });
            },
            Failed: response => {
                log.error(`getContentTypes (${model}) - Failed`, response);
            }
        });
    });
};
