/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {AccountType, ProcurifyAPI} from './procurifyapi';
import {Account} from '@extendapps/procurifyapi/Objects/Account';
import * as log from 'N/log';

export const performAccountTests = (procurifyAPI: ProcurifyAPI) => {
    const newAccount: Account = {
        code: 987,
        account_type: AccountType.INCOME,
        description: 'Performing API Tests',
        active: true,
        external_id: 'api_test_account'
    };

    procurifyAPI.createAccount({
        account: newAccount,
        Created: createdAccount => {
            log.debug('createAccount - Created', createdAccount);
            const modifiedAccount: Account = JSON.parse(JSON.stringify(createdAccount));
            modifiedAccount.description = 'I made a change';
            modifiedAccount.account_type = AccountType.INCOME; // TODO: Perhaps the API could ensure this is numeric.  If not, try and convert using enum.

            procurifyAPI.updateAccount({
                id: createdAccount.id,
                account: modifiedAccount,
                Updated: updatedAccount => {
                    if (updatedAccount.description === 'I made a change') {
                        log.debug('updateAccount - CHANGED!', updatedAccount);
                        procurifyAPI.deleteAccount({
                            id: updatedAccount.id,
                            Deleted: () => {
                                log.debug('deleteAccount - Deleted', '');
                                procurifyAPI.getAccounts({
                                    status: 'synced',
                                    OK: (allAccounts: Account[]) => {
                                        log.debug('getAccounts(synced) - OK', allAccounts);
                                    },
                                    Failed: response => {
                                        log.error('getAccounts(synced) - Failed', response);
                                    }
                                });
                            },
                            Failed: response => {
                                log.error('deleteAccount - Failed', response);
                            }
                        });
                    } else {
                        log.error('updatedAccount - NO CHANGE!', updatedAccount);
                    }
                },
                Failed: response => {
                    log.error('updatedAccount - Failed', response);
                }
            });
        },
        Failed: response => {
            log.error('createAccount - Failed', response);
        }
    });
};
