/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 * @NApiVersion 2.x
 * @NModuleScope Public
 * @NScriptType Suitelet
 */

import {EntryPoints} from 'N/types';
import * as https from 'N/https';
import {ProcurifyAPI} from './procurifyapi';
import * as log from 'N/log';
import {performAccountTests} from './AccountTests';
import {performVendorTests} from './VendorTests';
import {performPurchaseOrderTests} from './PurchaseOrderTests';
import {performItemReceiptLinesTests} from './ItemReceiptLinesTests';
import {performContentTypeTests} from './ContentTypeTests';

const testPFYDomain = {
    domain: 'extendapps0003.procurify.com',
    username: 'test@procurify.com',
    password: '12345678'
};

// noinspection JSUnusedGlobalSymbols
export let onRequest: EntryPoints.Suitelet.onRequest = (context: EntryPoints.Suitelet.onRequestContext) => {
    const requestRouter = {};
    requestRouter[https.Method.GET] = getHandler;

    requestRouter[context.request.method] ? requestRouter[context.request.method](context) : null;
};

// noinspection JSUnusedLocalSymbols
const getHandler = (context: EntryPoints.Suitelet.onRequestContext) => {
    log.audit('API TEST', 'START');
    const authString: string = ProcurifyAPI.getAuthorizationString(testPFYDomain.username, testPFYDomain.password);

    ProcurifyAPI.initialize({
        version: 'v3',
        domain: testPFYDomain.domain,
        auth_string: authString,
        OK: () => {
            log.debug('initialize - OK', '');
            const procurifyAPI: ProcurifyAPI = new ProcurifyAPI(testPFYDomain.domain, authString);

            switch (context.request.parameters.record) {
                case 'Account':
                case 'account':
                    performAccountTests(procurifyAPI);
                    break;

                case 'Vendor':
                case 'vendor':
                    performVendorTests(procurifyAPI);
                    break;

                case 'PO':
                case 'PurchaseOrder':
                    performPurchaseOrderTests(procurifyAPI);
                    break;

                case 'IR':
                case 'ItemRceiptLines':
                    performItemReceiptLinesTests(procurifyAPI);
                    break;

                case 'CT':
                case 'ContentTypes':
                    performContentTypeTests(procurifyAPI);
                    break;

                default:
                    log.error('Unsupported type', 'Please use record=Account|Vendor in the url to perform tests');
                    break;
            }
        },
        Failed: response => {
            log.error('initialize - Failed', response);
        }
    });
    log.audit('API TEST', 'END');
};
