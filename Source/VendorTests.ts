/**
 * @copyright 2019 ExtendApps, Inc.
 * @author Darren Hill darren@extendapps.com
 *
 */

import {ProcurifyAPI} from './procurifyapi';
import * as log from 'N/log';
import {Vendor} from '@extendapps/procurifyapi/Objects/Vendor';

export const performVendorTests = (procurifyAPI: ProcurifyAPI) => {
    const newVendor: Vendor = {
        name: 'API Test',
        address_line_one: '123 Main St',
        postal_code: '94062',
        city: 'Wooside',
        state_province: 'CA',
        country: 'United States',
        email: 'test@test.com',
        contact: 'Darren',
        phone: `6666666666`,
        external_id: 'api_test_vendor'
    };

    procurifyAPI.createVendor({
        vendor: newVendor,
        Created: createdVendor => {
            log.debug('createVendor - Created', createdVendor);
            const modifiedVendor: Vendor = JSON.parse(JSON.stringify(createdVendor));
            modifiedVendor.name = 'API Changed';

            procurifyAPI.updateVendor({
                id: createdVendor.id,
                vendor: modifiedVendor,
                Updated: updatedVendor => {
                    if (updatedVendor.name === 'API Changed') {
                        log.debug('updatedVendor - CHANGED!', updatedVendor);
                        procurifyAPI.deleteVendor({
                            id: updatedVendor.id,
                            Deleted: () => {
                                log.debug('deleteVendor - Deleted', '');
                            },
                            Failed: response => {
                                log.error('deleteVendor - Failed', response);
                            }
                        });
                    } else {
                        log.error('updatedVendor - NO CHANGE!', updatedVendor);
                    }
                },
                Failed: response => {
                    log.error('updateVendor - Failed', response);
                }
            });
        },
        Failed: response => {
            log.error('createVendor - Failed', response);
        }
    });
};
